﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;
using System.IO;

namespace InsertMongoDB
{
    class FileManager
    {
        private void add_M_QA_Data2DB(MongoDBMethod mdbm, M_QAA mqaa, int serilCount)
        {
            try
            {
                if (mqaa.ADD != null && mqaa.ADD != "")
                {
                    mqaa.title = serilCount.ToString("000");
                    mdbm.Add_QAA(mqaa);
                    Console.WriteLine("mqaa-title: " + mqaa.title);
                    Console.WriteLine("mqaa-Q: " + mqaa.Q);
                    Console.WriteLine("mqaa-A: " + mqaa.A);
                    Console.WriteLine("mqaa-ADD: " + mqaa.ADD);
                }
                else
                {
                    M_QA mqa = new M_QA();
                    mqa.title = serilCount.ToString("000");
                    mqa.Q = mqaa.Q;
                    mqa.A = mqaa.A;

                    mdbm.Add_QA(mqa);
                    Console.WriteLine("mqa-title: " + mqa.title);
                    Console.WriteLine("mqaa-Q: " + mqa.Q);
                    Console.WriteLine("mqaa-A: " + mqa.A);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int setCsvInfo2MongoDB(string filePath,string tableName,int serialCountStart)
        {
            try
            {
                string strconn = "Server=127.0.0.1";
                string dbName = "model1";
                string tempTableName = tableName;

                MongoDBMethod mdbm = new MongoDBMethod(strconn, dbName, tempTableName);

                if (!mdbm.openMongoDBConnect())
                {
                    Console.WriteLine("Can't open MongoDB。");
                    return 0;
                }                

                using (TextFieldParser parser = new TextFieldParser(filePath, Encoding.GetEncoding("shift_jis")))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.Delimiters = new string[] { "," };

                    int serilCount = serialCountStart;
 
                    while (!parser.EndOfData)
                    {
                        string[] parts = parser.ReadFields();                        

                        if (parts[0].Trim() == "Q")
                        {
                            if (parts[1].Trim() != "")
                            {
                                M_QAA mqaa = new M_QAA();
                                mqaa.Q = parts[1].Replace(" ", "").Replace("　", "");
                                parts = parser.ReadFields();
                                mqaa.A = parts[1].Replace(" ", "").Replace("　", "");

                                if (parts.Length == 5)
                                {
                                    mqaa.ADD = parts[4];
                                }

                                add_M_QA_Data2DB(mdbm, mqaa, serilCount);
                                serilCount++;
                            }
                        }
                    }

                    Console.WriteLine("finished!");

                    return serilCount;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
