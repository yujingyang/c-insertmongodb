﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;

namespace InsertMongoDB
{
    public class MongoDBMethod
    {
        MongoClient client;
        MongoServer server;
        MongoDatabase db;

        string strconn;
        string dbName;
        string tableName;

        public MongoDBMethod(string strconn, string dbName, string tableName)
        {
            this.strconn = strconn;
            this.dbName = dbName;
            this.tableName = tableName;

            client = new MongoClient(strconn);
            server = client.GetServer();
        }

        public bool openMongoDBConnect()
        {
            try
            {

                if (server.DatabaseExists(dbName))
                {

                    db = server.GetDatabase(dbName);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Add_QA(M_QA mqa)
        {
            MongoCollection mongoCollection = db.GetCollection(tableName);

            mongoCollection.Insert(mqa);
        }

        public void Add_QAA(M_QAA mqaa)
        {
            MongoCollection mongoCollection = db.GetCollection(tableName);

            mongoCollection.Insert(mqaa);
        }
    }
}
